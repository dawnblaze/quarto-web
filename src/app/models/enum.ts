export enum UserType {
    LandOwner = 1,
    Tenant = 2
}