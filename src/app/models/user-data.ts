export class UserData {
    ID: number;
    EmailAddress: string;
    PasswordChangeDT: Date;
    ResetPassword: boolean;
}